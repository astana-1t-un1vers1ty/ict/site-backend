package main

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/deliveos/ict-site-backend/models"
)

func main() {
	var port string = os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	r := gin.Default()
	r.LoadHTMLGlob("web/templates/**/*")
	r.Static("/web/public", "./web/public/")

	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.tmpl", gin.H{
			"title": "Recognition of cancerous tumors using neural networks",
		})
	})

	r.GET("/contacts", func(c *gin.Context) {
		c.HTML(http.StatusOK, "contacts.tmpl", models.ContactsData{
			Title: "Contacts",
			Contacts: []models.Contact{
				{
					Name:  "EDUARD",
					Phone: "+7 707 223 8002",
					Email: "edmikael2000@gmail.com",
				},
				{
					Name:  "ARUZHAN",
					Phone: "+7 777 727 8202",
					Email: "yumarukhan@gmail.com",
				},
			},
		})
	})

	r.GET("/jobs", func(c *gin.Context) {
		c.HTML(http.StatusOK, "jobs.tmpl", gin.H{
			"title": "Jobs",
		})
	})

	r.GET("/for-investors", func(c *gin.Context) {
		c.HTML(http.StatusOK, "for-investors.tmpl", gin.H{
			"title": "For investors",
		})
	})

	r.GET("/for-customers", func(c *gin.Context) {
		c.HTML(http.StatusOK, "for-customers.tmpl", gin.H{
			"title": "For customers",
		})
	})

	r.GET("/about", func(c *gin.Context) {
		c.HTML(http.StatusOK, "about.tmpl", gin.H{
			"title": "About us",
		})
	})

	r.GET("/applications/vacancy", func(c *gin.Context) {
		c.Redirect(http.StatusCreated, "/")
	})

	r.GET("/applications/investments", func(c *gin.Context) {
		c.Redirect(http.StatusCreated, "/")
	})

	r.GET("/applications/customers", func(c *gin.Context) {
		c.Redirect(http.StatusCreated, "/")
	})

	r.Run(":" + port)
}
