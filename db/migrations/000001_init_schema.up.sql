CREATE TABLE "types" (
  "id" serial PRIMARY KEY,
  "type_name" varchar NOT NULL 
);

CREATE TABLE "images" (
  "id" bigserial PRIMARY KEY,
  "image" bytea,
  "type_id" integer
);

ALTER TABLE "images" ADD FOREIGN KEY ("type_id") REFERENCES "types" ("id");

CREATE INDEX ON "types" ("type_name");
