package models

type ContactsData struct {
	Title    string
	Contacts []Contact
}

type Contact struct {
	Name  string
	Phone string
	Email string
}
